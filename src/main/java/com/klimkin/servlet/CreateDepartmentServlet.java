package com.klimkin.servlet;

import com.klimkin.dao.DepartmentDao;
import com.klimkin.dao.jdbc.JdbcDepartmentDao;
import com.klimkin.entity.Department;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/add_department")
public class CreateDepartmentServlet extends HttpServlet {
    Department department = new Department();
    DepartmentDao departmentDao = new JdbcDepartmentDao();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StringBuilder title = new StringBuilder("<h3 class=\"text-center\">");
        String dep = req.getParameter("dep");
        String button;
        String submit = "";

        if (dep == null) {
            title.append("Create Department").append("</h3>");
            button = "Create";
        } else {
            Integer id = Integer.parseInt(dep);
            title.append("Update department #").append(id).append("</h3>");
            button = "Update";
            submit ="?dep=" + dep;
        }

        if (req.getAttribute("value") == null) {
            req.setAttribute("value", "");
        }
        req.setAttribute("title", title);
        req.setAttribute("button", button);
        req.setAttribute("submit", submit);
        getServletContext().getRequestDispatcher("/add_department.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        department.setDepartmentName(req.getParameter("departmentName"));
        String dep = req.getParameter("dep");
        String message = "\n";
        String value = "";

        boolean compare = departmentDao.compare(department);

        if (dep == null) {
            if (compare) {
                message = "<p class=\"text-danger\">Department with the same name already exists.</p>";
                value = department.getDepartmentName();
                department = new Department();
            } else {
                message = "<p class=\"text-success\">Department successfully created.</p>";
                departmentDao.create(department);
            }
        } else {
            if (compare) {
                message = "<p class=\"text-danger\">Department with the same name already exists.</p>";
                value = department.getDepartmentName();
                department = new Department();
            } else {
                Integer id = Integer.parseInt(dep);
                department.setId(id);
                message = "<p class=\"text-success\">Department successfully updated.</p>";
                departmentDao.update(department);
            }
        }

        req.setAttribute("value", value);
        req.setAttribute("message", message);
        doGet(req, resp);
    }
}

