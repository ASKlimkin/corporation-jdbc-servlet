package com.klimkin.servlet;

import com.klimkin.dao.DepartmentDao;
import com.klimkin.dao.EmployeeDao;
import com.klimkin.dao.jdbc.JdbcDepartmentDao;
import com.klimkin.dao.jdbc.JdbcEmployeeDao;
import com.klimkin.entity.Department;
import com.klimkin.entity.Employee;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@WebServlet("/add_employee")
public class CreateEmployeeServlet extends HttpServlet {
    private Employee employee = new Employee();
    private EmployeeDao employeeDao = new JdbcEmployeeDao();
    private DepartmentDao departmentDao = new JdbcDepartmentDao();
    private String par;
    private String[] values = {"", "", "", "", ""};
    private boolean errorFlag = false;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StringBuilder title = new StringBuilder("<h3 class=\"text-center\">");
        par = req.getParameter("employee");
        String button;
        String submit = "";
        String back = "";

        if (par == null) {
            title.append("Create Employee").append("</h3>");
            button = "Create";
        } else {
            title.append("Update Employee #").append(par).append("</h3>");
            button = "Update";
            submit ="?employee=" + par;
            Integer dep_id = employeeDao.findEmployeeById(Integer.parseInt(par)).getDepartment().getId();
            back = "/main/staff?dep="+dep_id;
        }

        if (req.getAttribute("massage") == null) {
            this.values = new String[] {"", "", "", "", ""};
            String message = "<p class=\"text-dark\">Fill in the empty fields.</p>";
            req.setAttribute("message", message);
        }

        req.setAttribute("values", values);
        req.setAttribute("select", selectDepartment());
        req.setAttribute("back", back);
        req.setAttribute("title", title);
        req.setAttribute("button", button);
        req.setAttribute("submit", submit);
        getServletContext().getRequestDispatcher("/add_employee.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = null;

        if (par == null) {
            employee = fillEmployee(new Employee(), req);
            if (!errorFlag) {
                message = "<p class=\"text-success\">Employee successfully created.</p>";
                employeeDao.create(employee);
            } else {
                message = "<p class=\"text-danger\">Some fields is empty or employee with the same email already exists.</p>";
                employee = new Employee();
            }
        } else if (par != null) {
            Integer id = Integer.parseInt(par);
            employee = fillEmployee(employeeDao.findEmployeeById(id), req);;
            if (!errorFlag) {
                message = "<p class=\"text-success\">Employee successfully updated.</p>";
                employeeDao.update(employee);
            } else {
                message = "<p class=\"text-danger\">Employee with the same email already exists.</p>";
                employee = new Employee();
            }
        }

        req.setAttribute("message", message);
        doGet(req, resp);
    }

    private String selectDepartment() {
        StringBuilder select = new StringBuilder();
        List<Department> departments = departmentDao.departmentsList();
        for (Department d : departments) {
            select.append("<option value=\"").append(d.getId()).append("\">").append(d.getDepartmentName()).append("</option>\n");
        }
        return select.toString();
    }

    private Employee fillEmployee(Employee employee, HttpServletRequest req) {
        this.errorFlag = false;
        String state;
        Employee jack = employee;
        boolean isNewJack = jack.getId() == null;
        String[] statesError = new String[5];

        state = req.getParameter("firstName");
        if (state == null || state.trim().equals("")){
            if (isNewJack) {
                this.errorFlag = true;
            } else {
                jack.setFirstName(null);
            }
            statesError[0] = "";
        } else {
            checkString(state);
            jack.setFirstName(state.trim());
            statesError[0] = state;
        }

        state = req.getParameter("lastName");
        if (state == null || state.trim().equals("")){
            if (isNewJack) {
                this.errorFlag = true;
            }
            jack.setLastName(null);
            statesError[1] = "";
        } else {
            checkString(state);
            jack.setLastName(state.trim());
            statesError[1] = state;
        }

        state = req.getParameter("email");
        if (state == null || state.trim().equals("")){
            if (isNewJack) {
                this.errorFlag = true;
            } else {
                jack.setLastName(null);
            }
            statesError[2] = "";
        } else {
            checkEmail(state);
            if (employeeDao.compare(state)) {
                this.errorFlag = true;
            }
            jack.setLastName(state);
            statesError[2] = state;
        }

        state = req.getParameter("birthday");
        if (state == null || state.trim().equals("")){
            if (isNewJack) {
                this.errorFlag = true;
            }
            jack.setFirstName(null);
            statesError[3] = "";
        } else {
            try {
                jack.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse(state));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            statesError[3] = state;
        }

        state = req.getParameter("salary");
        if (state == null || state.trim().equals("")){
            if (isNewJack) {
                this.errorFlag = true;
            }
            jack.setSalary(null);
            statesError[4] = "";
        } else {
            if (lookNumber(state) == null) {
                jack.setSalary(Integer.parseInt(state));
            }
            statesError[4] = state;

        }

        state = req.getParameter("department_id");
        if (state == null || state.equals("")){
            jack.setDepartment(null);
        } else {
            jack.setDepartment(departmentDao.findDepartmentById(Integer.parseInt(state)));
        }

        if (errorFlag) {
            this.values = statesError;
        }
        return jack;
    }

    private void checkString(String state) {
        int mustBeZero = state
                .replaceAll("\\p{Alpha}", "")
                .replaceAll(" ", "")
                .length();
        if (mustBeZero != 0) {
            errorFlag = true;
        }
    }

    private void checkEmail(String state) {
        int mustBeZero = 1;
        try {
            mustBeZero = state
                    .split("@")[1]
                    .split(".")[1]
                    .replaceAll("\\p{Alpha}", "")
                    .length();
        } catch (Exception e) {
            errorFlag = true;
        }
        if (mustBeZero != 0) {
            errorFlag = true;
        }
}

    private String lookNumber(String state) {
        int mustBeZero = state
                .replaceAll("\\p{Digit}", "")
                .length();
        if (mustBeZero != 0) {
            errorFlag = true;
            return null;
        } else {
            return state;
        }
    }
}

