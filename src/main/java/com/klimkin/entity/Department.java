package com.klimkin.entity;

import com.klimkin.dao.EmployeeDao;
import com.klimkin.dao.jdbc.JdbcEmployeeDao;

public class Department {

    private Integer id;
    private String departmentName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

}
